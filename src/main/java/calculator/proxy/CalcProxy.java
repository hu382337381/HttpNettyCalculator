package calculator.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by liyf on 2015/3/30 0030.
 */
public class CalcProxy {
    protected final static Logger logger = LoggerFactory.getLogger(CalcProxy.class);

    private CalcProxy calculator;
    private static CalcProxy singleton;
    public CalcProxy(){

    }
    public static CalcProxy instance(){
        if (singleton == null){
            singleton = new CalcProxy();
        }
        return singleton;
    }

    public <T>T invoke(Calculable calculable){
        long s = System.currentTimeMillis();
        logger.info("计算开始：");
        T t = (T) calculable.calc();
        long e = System.currentTimeMillis();
        logger.info("计算结束:耗时 "+ (e-s)/1000.0 +"s");
        return t;
    }
    
}
