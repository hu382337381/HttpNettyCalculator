package calculator.proxy;

/**
 * Created by liyf on 2015/3/30 0030.
 */
public interface Calculable<I> {
    public I calc();
}
